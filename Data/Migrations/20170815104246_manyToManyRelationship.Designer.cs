﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Data;

namespace Data.Migrations
{
    [DbContext(typeof(MyDatabaseContext))]
    [Migration("20170815104246_manyToManyRelationship")]
    partial class manyToManyRelationship
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("Domain.Groups", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("varchar")
                        .HasMaxLength(500);

                    b.Property<bool>("IsActive")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("is_active")
                        .HasDefaultValueSql("true");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("varchar")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("cfg_groups");
                });

            modelBuilder.Entity("Domain.Permissions", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("varchar")
                        .HasMaxLength(500);

                    b.Property<bool>("IsActive")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("is_active")
                        .HasDefaultValueSql("true");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("varchar")
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("cfg_permissions");
                });

            modelBuilder.Entity("Domain.PermissionsInGroup", b =>
                {
                    b.Property<int>("GroupId")
                        .HasColumnName("group_id");

                    b.Property<int>("PermissionId")
                        .HasColumnName("permission_id");

                    b.HasKey("GroupId", "PermissionId");

                    b.HasIndex("PermissionId");

                    b.ToTable("cfg_permissions_in_group");
                });

            modelBuilder.Entity("Domain.PermissionsInGroup", b =>
                {
                    b.HasOne("Domain.Groups", "Group")
                        .WithMany("PermissionsInGroup")
                        .HasForeignKey("GroupId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Domain.Permissions", "Permission")
                        .WithMany("PermissionsInGroup")
                        .HasForeignKey("PermissionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
