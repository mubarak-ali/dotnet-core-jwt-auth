﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Data.Migrations
{
    public partial class manyToManyRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Permissions",
                table: "Permissions");

            migrationBuilder.RenameTable(
                name: "Permissions",
                newName: "cfg_permissions");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "cfg_permissions",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "IsActive",
                table: "cfg_permissions",
                newName: "is_active");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "cfg_permissions",
                newName: "description");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "cfg_permissions",
                newName: "id");

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "cfg_permissions",
                type: "varchar",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "is_active",
                table: "cfg_permissions",
                nullable: false,
                defaultValueSql: "true",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<string>(
                name: "description",
                table: "cfg_permissions",
                type: "varchar",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_cfg_permissions",
                table: "cfg_permissions",
                column: "id");

            migrationBuilder.CreateTable(
                name: "cfg_groups",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    description = table.Column<string>(type: "varchar", maxLength: 500, nullable: true),
                    is_active = table.Column<bool>(nullable: false, defaultValueSql: "true"),
                    name = table.Column<string>(type: "varchar", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cfg_groups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "cfg_permissions_in_group",
                columns: table => new
                {
                    group_id = table.Column<int>(nullable: false),
                    permission_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cfg_permissions_in_group", x => new { x.group_id, x.permission_id });
                    table.ForeignKey(
                        name: "FK_cfg_permissions_in_group_cfg_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "cfg_groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_cfg_permissions_in_group_cfg_permissions_permission_id",
                        column: x => x.permission_id,
                        principalTable: "cfg_permissions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_cfg_permissions_in_group_permission_id",
                table: "cfg_permissions_in_group",
                column: "permission_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "cfg_permissions_in_group");

            migrationBuilder.DropTable(
                name: "cfg_groups");

            migrationBuilder.DropPrimaryKey(
                name: "PK_cfg_permissions",
                table: "cfg_permissions");

            migrationBuilder.RenameTable(
                name: "cfg_permissions",
                newName: "Permissions");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Permissions",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "is_active",
                table: "Permissions",
                newName: "IsActive");

            migrationBuilder.RenameColumn(
                name: "description",
                table: "Permissions",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Permissions",
                newName: "Id");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Permissions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Permissions",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "true");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Permissions",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar",
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Permissions",
                table: "Permissions",
                column: "Id");
        }
    }
}
