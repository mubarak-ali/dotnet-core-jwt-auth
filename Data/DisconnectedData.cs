﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Data
{
    public class DisconnectedData
    {
        private MyDatabaseContext _context;

        public DisconnectedData(MyDatabaseContext context)
        {
            _context = context;
            _context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public IEnumerable<Groups> GetList()
        {
            return _context.Groups.ToList();
        }

        public Groups GetById(int id)
        {
            var group = _context.Groups.Include(g => g.GroupDetails).FirstOrDefault(g => g.Id == id);
            return group;
        }

        public void SaveEntity(Groups group)
        {
            _context.ChangeTracker.TrackGraph(group, e => ApplyStateUsingIsKeySet(e.Entry));
            _context.SaveChanges();
        }

        public void DeleteSamuraiGraph(int id)
        {
            //goal:  delete samurai , quotes and secret identity
            //       also delete any joins with battles
            //EF Core supports Cascade delete by convention
            //Even if full graph is not in memory, db is defined to delete
            //But always double check!
            var group = _context.Groups.Find(id); //NOT TRACKING !!
            _context.Entry(group).State = EntityState.Deleted; //TRACKING
            _context.SaveChanges();
        }

        private static void ApplyStateUsingIsKeySet(EntityEntry entry)
        {
            if (entry.IsKeySet)
            {
                entry.State = EntityState.Modified;
            }
            else
            {
                entry.State = EntityState.Added;
            }
        }
    }
}