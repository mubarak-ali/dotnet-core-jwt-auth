﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class MyDatabaseContext : DbContext
    {
        public DbSet<Permissions> Permissions { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<PermissionsInGroup> PermissionsInGroup { get; set; }
        public DbSet<GroupDetails> GroupDetails { get; set; }

        public MyDatabaseContext(DbContextOptions<MyDatabaseContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql("");
            //optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PermissionsInGroup>(entity =>
            {
                entity.ToTable("cfg_permissions_in_group");

                entity.HasKey(p => new { p.GroupId, p.PermissionId });

                entity.Property(p => p.GroupId)
                .HasColumnName("group_id");

                entity.Property(p => p.PermissionId)
                .HasColumnName("permission_id");
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.ToTable("cfg_groups");

                entity.HasKey(g => g.Id);

                entity.Property(g => g.Id)
                .HasColumnName("id");

                entity.Property(g => g.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255);

                entity.Property(g => g.Description)
                .HasColumnName("description")
                .HasColumnType("varchar")
                .HasMaxLength(500);

                entity.Property(g => g.IsActive)
                .HasColumnName("is_active")
                .HasDefaultValueSql("true");
            });

            modelBuilder.Entity<Permissions>(entity =>
            {
                entity.ToTable("cfg_permissions");

                entity.HasKey(g => g.Id);

                entity.Property(g => g.Id)
                .HasColumnName("id");

                entity.Property(g => g.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasColumnType("varchar")
                .HasMaxLength(255);

                entity.Property(g => g.Description)
                .HasColumnName("description")
                .HasColumnType("varchar")
                .HasMaxLength(500);

                entity.Property(g => g.IsActive)
                .HasColumnName("is_active")
                .HasDefaultValueSql("true");
            });

            modelBuilder.Entity<GroupDetails>(entity =>
            {
                entity.ToTable("group_details");

                entity.HasKey(g => g.Id);

                entity.Property(g => g.Id)
                .HasColumnName("id");

                entity.Property(g => g.GroupId)
                .HasColumnName("group_id");

                entity.Property(g => g.Title)
                .HasColumnName("title")
                .HasColumnType("varchar")
                .HasMaxLength(255);

                entity.Property(g => g.Type)
                .HasColumnName("group_type")
                .HasColumnType("varchar")
                .HasMaxLength(20);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}