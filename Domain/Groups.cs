﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class Groups
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public List<PermissionsInGroup> PermissionsInGroup { get; set; }
        public GroupDetails GroupDetails { get; set; }

    }
}
