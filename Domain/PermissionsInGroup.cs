﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class PermissionsInGroup
    {
        public int PermissionId { get; set; }
        public int GroupId { get; set; }

        public Permissions Permission { get; set; }
        public Groups Group { get; set; }

    }
}
