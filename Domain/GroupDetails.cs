﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class GroupDetails
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }

        public int GroupId { get; set; }
        public Groups Group { get; set; }
    }
}
