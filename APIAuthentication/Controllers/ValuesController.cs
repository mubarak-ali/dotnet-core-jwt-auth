﻿using Data;
using Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace APIAuthentication.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private DisconnectedData _repo;

        public ValuesController(DisconnectedData repo)
        {
            _repo = repo;
        }

        //[Authorize]
        [HttpGet]
        public IEnumerable<Groups> Get()
        {
            //var id = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var list = _repo.GetList();
            return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Groups Get(int id)
        {
            return _repo.GetById(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]Groups group)
        {
            _repo.SaveEntity(group);
        }

        // PUT api/values/5
        [HttpPut]
        public void Put([FromBody]Groups group)
        {
            _repo.SaveEntity(group);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}