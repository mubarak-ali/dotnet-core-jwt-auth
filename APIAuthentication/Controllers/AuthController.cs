using APIAuthentication.Extensions;
using APIAuthentication.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace APIAuthentication.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private TokenOptions Options { get; }

        public AuthController(IOptions<TokenOptions> options)
        {
            Options = options.Value;
        }

        [HttpPost("token")]
        public IActionResult Token([FromBody] TokenRequest request)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, Guid.NewGuid().ToString().ToUpper()),
                new Claim(ClaimTypes.Name, request.Username),
                new Claim(ClaimTypes.Email, request.Email)
            };

            var token = new JwtSecurityToken(
                audience: Options.Audience,
                issuer: Options.Issuer,
                expires: Options.GetExpiration(),
                claims: claims,
                signingCredentials: Options.GetSigningCredentials());

            return Ok(
                new TokenResponse
                {
                    token_type = Options.Type,
                    access_token = new JwtSecurityTokenHandler().WriteToken(token),
                    expires_in = (int)Options.ValidFor.TotalMinutes
                });
        }
    }
}