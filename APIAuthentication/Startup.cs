﻿using APIAuthentication.Extensions;
using APIAuthentication.Models;
using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace APIAuthentication
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<TokenOptions>(Configuration.GetSection(nameof(TokenOptions)));

            // Add framework services.
            services.AddDbContext<MyDatabaseContext>(options => 
            {
                options.UseNpgsql(Configuration.GetConnectionString("MyAppDb"));
            });
            services.AddScoped<DisconnectedData>();

            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var options = Configuration.GetSection(nameof(TokenOptions)).Get<TokenOptions>();

            app.UseJwtBearerAuthentication(new JwtBearerOptions()
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    ValidAudience = options.Audience,
                    ValidIssuer = options.Issuer,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = options.GetSymmetricSecurityKey()
                },
                Events = new JwtBearerEvents()
                {
                    //OnTokenValidated = (context) =>
                    //{
                    //    /*Validate it with someone*/
                    //    if (false)
                    //    {
                    //        context.SkipToNextMiddleware();
                    //    }
                    //    return Task.FromResult(0);
                    //}
                }
            });

            app.UseMvc();
        }
    }
}